local opt = vim.opt

-- Editor options
opt.autowrite = true -- Enable auto write
opt.expandtab = true -- Use spaces instead of tabs
opt.tabstop = 4 -- Number of spaces tabs count for
opt.shiftwidth = 4
opt.number = true -- Relative line numbers
opt.relativenumber = true -- Relative line numbers
opt.shiftround = true -- Round indent
opt.cursorline = true -- Enable highlighting of the current line
opt.termguicolors = true -- True color support
if vim.fn.has("nvim-0.10") == 1 then
  opt.smoothscroll = true --enable smoothscrolling
end

