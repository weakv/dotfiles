local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  -- bootstrap lazy.nvim
  -- stylua: ignore
  vim.fn.system({ "git", "clone", "--filter=blob:none", "https://github.com/folke/lazy.nvim.git", "--branch=stable", lazypath })
end
vim.opt.rtp:prepend(vim.env.LAZY or lazypath)

require("lazy").setup("plugins")
require("mason").setup()
require("mason-lspconfig").setup {
    ensure_installed = { "lua_ls", "pyright", "tflint", "gopls"},
}
 require("mason-lspconfig").setup_handlers({
  ["pyright"] = function() -- specific configuration for Pyright
    require("lspconfig").pyright.setup({
      -- server-specific settings here
    })
    require("lspconfig").terraformls.setup({
        on_attach = on_attach,
        flags = { debounce_text_changes = 150 },
        capabilities = capabilities,
    })
    require('lspconfig').tflint.setup({
        on_attach = on_attach,
        flags = { debounce_text_changes = 150 },
    })
    require'lspconfig'.gopls.setup({
     -- server-specific settings here
    })
  end,
})

local cmp = require('cmp')
local luasnip = require('luasnip')

cmp.setup({
  sources = {
    {name = 'nvim_lsp'},
  },
  mapping = {
    ['<C-i>'] = cmp.mapping(function(fallback)
        if cmp.visible() then 
            cmp.confirm({
                behavior = cmp.ConfirmBehavior.Replace,
                select = true,
            })
        else
            fallback()
        end
    end, {'i', 's'}),
    ['<C-Space>'] = cmp.mapping.complete(),
    ['<C-u>'] = cmp.mapping.scroll_docs(-4),
    ['<C-d>'] = cmp.mapping.scroll_docs(4),
  },
  snippet = {
    expand = function(args)
      luasnip.lsp_expand(args.body)
    end
  },
})
