local map = vim.api.nvim_set_keymap
vim.g.mapleader = ' '

-- Telsecope commands
map('n', '<leader>f', ':Telescope find_files<CR>', {noremap = true, silent = false})
map('n', '<leader>fg', ':Telescope live_grep<CR>', {noremap = true, silent = false})
-- Remote plugin commands
map('n', '<leader>rs', ':RemoteStart<CR>',{noremap = true, silent = false})
-- Git related commands
map('n', '<leader>gs', ':G status<CR>',{noremap = true, silent = false})
map('n', '<leader>ga.', ':G add .<CR>',{noremap = true, silent = false})
map('n', '<leader>gc', ':G commit<CR>',{noremap = true, silent = false})
map('n', '<leader>gui', ':Neogit<CR>',{noremap = true, silent = false})
-- Default editor commands
map('n', '<leader>w', ':w<CR>',{noremap = true, silent = false})
map('n', '<leader>Q', ':qa!<CR>',{noremap = true, silent = false})
map('n', '<leader>e', ':Explore<CR>',{noremap = true, silent = false})
map('n', '<leader>t', ':tabnew<CR>',{noremap = true, silent = false})
map('n', '<leader>tl', ':tabnext<CR>',{noremap = true, silent = false})
map('n', '<leader>th', ':tabprev<CR>',{noremap = true, silent = false})

-- Debugger
map('n', '<leader>db', ':DapToggleBreakpoint <CR>',{noremap = true, silent = false})
map('n', '<leader>dr', ':DapContinue <CR>',{noremap = true, silent = false})
