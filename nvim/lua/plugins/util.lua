return{
        {
                "NeogitOrg/neogit",
                dependencies = {
                        "nvim-lua/plenary.nvim",         -- required
                        "sindrets/diffview.nvim",        -- optional - Diff integration
                        "nvim-telescope/telescope.nvim", -- optional
                },
                config = true
        },
        {
                "tpope/vim-fugitive"
        },
        {
                'nvimdev/dashboard-nvim',
                event = 'VimEnter',
                config = function()
                require('dashboard').setup {
                -- config
                }
                end,
                dependencies = { {'nvim-tree/nvim-web-devicons'}}
        }
}
