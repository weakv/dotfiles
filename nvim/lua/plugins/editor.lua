return{
	"nvim-telescope/telescope-file-browser.nvim",
  lazy = false,
	dependencies = { "nvim-telescope/telescope.nvim", "nvim-lua/plenary.nvim" }
}
