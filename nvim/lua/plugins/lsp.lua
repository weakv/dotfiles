return{
    {
        "williamboman/mason-lspconfig.nvim",
        dependencies = {
            "williamboman/mason.nvim",
            "williamboman/mason-lspconfig.nvim",
            "neovim/nvim-lspconfig",
        },
        config = function()
        require("mason-lspconfig").setup()
        end,
    },
    {'hrsh7th/nvim-cmp'},                  -- Autocomplete engine
    {'hrsh7th/cmp-nvim-lsp'},              -- Completion source for LSP
    {'L3MON4D3/LuaSnip'},                  -- Snippet engine
}
